CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The OpenAI Moderation Augmentor is a submodule of Augmentor.

It allows Augmentor to interface with the <a href="https://platform.openai.com/docs/api-reference/moderations" title="OpenAI Moderation API link">OpenAI Moderation API</a>.

Full results are available in the output either 
1. json encoded for storage in `$output['encoded_results']`
2. the full results in `$output['results']`
3. a flag, whether or not it was classified as violating in `$output['flagged']`

```
        $parsed = reset($result['results']);
        $output['encoded_results'] = json_encode($parsed);
        $output['results'] = $parsed;
        $output['flagged'] = (bool) $parsed['flagged'];

        return $output;
```

REQUIREMENTS
------------

This module requires the following modules:

 * [Augmentor](https://www.drupal.org/project/augmentor):


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * Configure the user permissions in Administration » People » Permissions:

   - Administer augmentors

     Users with this permission will see the webservices > augmentors
     configuration list page. From here they can add, configure, delete, enable
     and disabled augmentors.

     Warning: Give to trusted roles only; this permission has security
     implications. Allows full administration access to create and edit
     augmentors.


MAINTAINERS
-----------

Current maintainers:
 * Ronald te Brake (ronaldtebrake) - https://www.drupal.org/u/ronaldtebrake
