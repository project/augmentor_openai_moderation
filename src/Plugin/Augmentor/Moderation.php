<?php

namespace Drupal\augmentor_moderation\Plugin\Augmentor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Component\Serialization\Json;
use Drupal\augmentor_moderation\ModerationBase;

/**
 * OpenAI Moderation augmentor plugin implementation.
 *
 * @Augmentor(
 *   id = "moderation",
 *   label = @Translation("Moderation"),
 *   description = @Translation("Given a input text, outputs if the model classifies it as violating OpenAI's content policy."),
 * )
 */
class Moderation extends ModerationBase {

  use DependencySerializationTrait;

  /**
   * Default engine to use when we don't have access to the API.
   */
  const DEFAULT_MODEL = 'text-moderation-latest';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'model' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['model'] = [
      '#type' => 'select',
      '#title' => $this->t('Model'),
      '#description' => $this->t("Currently, only text-moderation-latest and text-moderation-stable are <a href='https://platform.openai.com/docs/api-reference/moderations/object' target='_blank'>supported</a>."),
      '#options' => [
        'text-moderation-latest' => 'text-moderation-latest',
        'text-moderation-stable' => 'text-moderation-stable',
      ],
      '#default_value' => $this->configuration['model'] ?? self::DEFAULT_MODEL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['model'] = $form_state->getValue('model');
  }

  /**
   * Creates a Moderation request.
   *
   * @param string $input
   *   The text to use as source for the completion manipulation.
   *
   * @return array
   *   The moderation output.
   */
  public function execute(string $input): array {
    $options = [
      'model' => $this->configuration['model'],
      'input' => $input,
    ];

    try {
      $result = Json::decode($this->getClient()->moderation($options), TRUE);

      if (array_key_exists('_errors', $result)) {
        $this->logger->error('OpenAI API error: %message.', [
          '%message' => $result['_errors']['message'],
        ]);

        return [
          '_errors' => $this->t('Error during the moderation execution, please check the logs for more information.')->render(),
        ];
      }
      else {
        $parsed = reset($result['results']);
        $output['encoded_results'] = json_encode($parsed);
        $output['results'] = $parsed;
        $output['flagged'] = (bool) $parsed['flagged'];
      }
    }
    catch (\Throwable $error) {
      $this->logger->error('OpenAI API error: %message.', [
        '%message' => $error->getMessage(),
      ]);
      return [
        '_errors' => $this->t('Error during the chat completion execution, please check the logs for more information.')->render(),
      ];
    }

    return $output;
  }

}
